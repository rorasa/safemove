###########################################################
#  SafeMove
#  Author: Wattanit Hotrakool
#  Copyright: 2018 Wattanit Hotrakool
#  License: MIT
###########################################################

import os, os.path
import shutil
import hashlib
import sys

if len(sys.argv)>2:
    DESTINATION = sys.argv[2]
else:
    DESTINATION="/Users/ton/Desktop/safemove"

if len(sys.argv)>1:
    TARGET = sys.argv[1]
else:
    TARGET="/Users/ton/Desktop/test"

# Create destination directory
if not os.path.isdir(DESTINATION):
    os.makedirs(DESTINATION)

class SafeMove:
    copiedfiles = 0
    totalfiles = 0
    verifiedfiles = 0

    def __init__(self, TARGET):
        # Count file to be copied
        for root,dir,files in os.walk(TARGET):
            for f in files:
                self.totalfiles += 1

    def copy(self, target, destination, filename):
        org_path = os.path.join(target, filename)
        des_path = os.path.join(destination, filename)

        if os.path.isfile(org_path):
            print("############")
            print("COPYING "+org_path+" to "+des_path)
            shutil.copyfile(org_path, des_path)
            self.copiedfiles += 1
            print("...Done : Copied "+str(self.copiedfiles)+ " from "+str(self.totalfiles)+ " files")

            org_hash = self.getSHA256(org_path)
            des_hash = self.getSHA256(des_path)

            if org_hash == des_hash:
                print("...Verified : SHA256 of "+org_path+" matches "+des_path)
                self.verifiedfiles += 1

        if os.path.isdir(org_path):
            # Create destination directory
            if not os.path.isdir(des_path):
                os.makedirs(des_path)

            for name in os.listdir(org_path):
                self.copy(org_path, des_path, name)

    def getSHA256(self, path):
        sha256 = hashlib.sha256()
        with open(path, 'rb') as f:
            for block in iter(lambda: f.read(65536), b''):
                sha256.update(block)
        return sha256.hexdigest()

    def cleanTarget(self, target):
        if self.verifiedfiles == self.copiedfiles:
            print("DELETING ORIGINAL FILES.")
            shutil.rmtree(target)
            os.makedirs(target)
            print("...Done")
        else:
            raise RuntimeError("Destination files do not match the original. Please copy again.")

if __name__=="__main__":
    # Create new DeepCopy
    safemove = SafeMove(TARGET)

    # Copy all files to DESTINATION
    for name in os.listdir(TARGET):
        try:
            safemove.copy(TARGET, DESTINATION, name)
        except:
            print("Failed to copy "+os.path.join(TARGET, name))
            continue

    # Delete the original files
    safemove.cleanTarget(TARGET)
